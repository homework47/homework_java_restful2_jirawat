package com.shop.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import static org.springframework.boot.SpringApplication.run;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })

public class TestApp {

	public static void main(String[] args) {
		run(TestApp.class, args);
		System.out.println("Hello world!!");
	}
}
 