package com.shop.demo.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;
import java.util.logging.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import org.springframework.web.bind.annotation.RestController;


import com.shop.demo.model.Manager;
import com.shop.demo.model.Sale;
import com.shop.demo.model.Shop;
import com.shop.demo.repo.ManagerRepository;
import com.shop.demo.repo.SaleRepository;
import com.shop.demo.repo.ShopRepository;

@Controller 
public class MainController {
	private static final Logger logger = Logger.getLogger("InputFieldException.class");
	FileHandler fh;

	private static final String TEMPLATE = "Hello, %s!";
	private static final String TEMPLATE_ADMIN = "Hello Admin, %s!";
	private final AtomicLong counter = new AtomicLong();

	@Autowired
	private ShopRepository shopRepository;
	@Autowired
	private SaleRepository saleRepo;
	@Autowired
	private ManagerRepository managerRepo;

	@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Input field is wrong")
	@ExceptionHandler(InputFieldException.class)
	public void handleInputFieldException() {
		try {
			fh = new FileHandler("D:/internship gosoft/basic learning/MyLogFile.log");
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			logger.info("My first log");

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info("Hi How r u?");
		System.out.println("Handle Input field Exception");
		logger.info("This is info");
	}

	@PostMapping(path = "/shop/add", consumes = { MediaType.APPLICATION_JSON_VALUE }) 
	public @ResponseBody String addNewShop(@RequestBody Shop shop) {
Set<Sale> sale = new HashSet<Sale>();

		Sale sale1 = new Sale();
		sale1.setName("PC");
		sale1.setCost(15000);
		sale.add(sale1);

		Sale sale2 = new Sale();
		sale2.setName("Phone");
		sale2.setCost(9000);
		sale.add(sale2);

		shopRepository.save(shop);
		sale1.setShop(shop);
		sale2.setShop(shop);
		saleRepo.save(sale1);
		saleRepo.save(sale2);
		shop.setSale(sale);

		shopRepository.save(shop);
		return "Add : " + shop.getShop();
	}

	@GetMapping(path = "/shop/all")
	public @ResponseBody Iterable<Shop> getAllShop() {
		return shopRepository.findAll();
	}

	@PutMapping(path = "/shop/edit", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String editShop(@RequestBody Shop shop) {
		shop.setStaff(shop.getStaff());
		shop.setComputer(shop.getComputer());
		shop.setManager(shop.getManager());
		shopRepository.save(shop);
		return "Save";
	}

	@DeleteMapping("/shop/deleteShop/{shop_id}")
	@ResponseBody
	String deleteUser(@PathVariable("shop_id") int shopId) {
		if (!shopRepository.findById(shopId).isEmpty()) {
			shopRepository.deleteById(shopId);
		}
		return "Delete User : " + shopId;
	}

	@GetMapping(path = "/manager/all")
	public @ResponseBody Iterable<Manager> getAllManager() {
		return managerRepo.findAll();
	}
	@PutMapping(path = "/manager/edit", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String editManager(@RequestBody Manager manager) {
		manager.setName(manager.getName());
		managerRepo.save(manager);
		return "Save";
	}

	@DeleteMapping("/manager/delete/{manager_id}")
	@ResponseBody
	String delete(@PathVariable("manager_id") int managerId) {
		if (!managerRepo.findById(managerId).isEmpty()) {
			managerRepo.deleteById(managerId);
		}
		return "Delete User : " + managerId;
	}   

	@PostMapping(path = "/manager/add", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String addNewManager(@RequestBody Manager manager) {
		managerRepo.save(manager);
		return "Add : " + manager;
	}
}
